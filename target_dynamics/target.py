"""Dynamics target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th
from target_dynamics.sinks import (
    DynamicsSink,
)


class TargetDynamics(Target):
    """Sample target for Dynamics."""
    

    name = "target-dynamics"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True
           
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True
           
        ),
        th.Property(
            "redirect_uri",
            th.StringType,
            required=True
           
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True
           
        ),
        
    ).to_dict()
    default_sink_class = DynamicsSink

if __name__ == '__main__':
    TargetDynamics.cli()
