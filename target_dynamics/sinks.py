"""Dynamics target sink class, which handles writing streams."""

import requests
import json
from datetime import datetime, timedelta
from requests import session
from singer_sdk.sinks import RecordSink
from singer_sdk.plugin_base import PluginBase
from typing import Dict, List, Optional
from target_dynamics.mapping import UnifiedMapping
import singer
LOGGER = singer.get_logger()

class DynamicsAuth(requests.auth.AuthBase):
    def __init__(self, config):
        self.__config = config
        # self.__config_path = parsed_args.config_path
        self.__resource = "https://{}.crm.dynamics.com".format(config["org"])
        self.__client_id = config["client_id"]
        self.__client_secret = config["client_secret"]
        self.__redirect_uri = config["redirect_uri"]
        self.__refresh_token = config["refresh_token"]

        self.__session = requests.Session()
        self.__access_token = None
        self.__expires_at = None

    def ensure_access_token(self):
        if self.__access_token is None or self.__expires_at <= datetime.utcnow():
            response = self.__session.post(
                "https://login.microsoftonline.com/common/oauth2/token",
                data={
                    "client_id": self.__client_id,
                    "client_secret": self.__client_secret,
                    "redirect_uri": self.__redirect_uri,
                    "refresh_token": self.__refresh_token,
                    "grant_type": "refresh_token",
                    "resource": self.__resource,
                },
            )

            if response.status_code != 200:
                raise Exception(response.text)

            data = response.json()

            self.__access_token = data["access_token"]
            self.__config["refresh_token"] = data["refresh_token"]
            self.__config["expires_in"] = data["expires_in"]
            self.__config["access_token"] = data["access_token"]

            with open("config.json", "w") as outfile:
                json.dump(self.__config, outfile, indent=4)

            self.__expires_at = datetime.utcnow() + timedelta(
                seconds=int(data["expires_in"]) - 10
            )  # pad by 10 seconds for clock drift

    def __call__(self, r):
        self.ensure_access_token()
        r.headers["Authorization"] = "Bearer {}".format(self.__access_token)
        return r

class DynamicsSink(RecordSink):
    """Dynamics target sink class."""
    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)
        # Save config for refresh_token saving
        self.config_file = target.config
        self.target_name = "dynamics"
        if self.config.get("full_url"):
            base_url = self.config["full_url"]
        else:
            base_url = "https://{}.crm.dynamics.com".format(self.config["org"])
        
        if base_url.endswith("/"):
            base_url = base_url[:-1]
        
        self.url = f"{base_url}/api/data/v9.2/"

    def get_auth(self):
        auth = DynamicsAuth(dict(self.config))
        r = requests.Session()
        auth = auth(r)
        return auth

    def contact_upload(self, record):
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record,"contacts",self.target_name)
        auth = self.get_auth()
        url =  self.url
        url = f"{url}contacts"
        auth.headers.update({"OData-MaxVersion":"4.0","OData-Version":"4.0","Accept":"application/json","Content-Type":"application/json","charset":"utf-8","Prefer":"return=representation"})
        res = auth.post(url,json=payload)
        if res.status_code not in [200, 201]:
            raise Exception(f"{res.status_code} {res.reason} {res.text}")
        LOGGER.info(res.text)      

    def deals_upload(self, record):
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record,"opportunity",self.target_name)
        auth = self.get_auth()
        url =  self.url
        # Get request to obtain contact_id to associate with this opportunity record
        contact_url = f"{url}contacts?$select=_msdyn_contactkpiid_value, emailaddress1&$filter=emailaddress1 eq '{payload['emailaddress']}'"
        contact_res = json.loads(auth.get(contact_url).text)

        url = f"{url}opportunities"
        auth.headers.update({"OData-MaxVersion":"4.0","OData-Version":"4.0","Accept":"*/*","Content-Type":"application/json","charset":"utf-8","Prefer":"return=representation", 'Accept-Encoding': 'gzip, deflate'})
        if len(contact_res["value"][0]) > 0:
            contact = contact_res["value"][0]["contactid"]
            payload["customerid_contact@odata.bind"] = f"/contacts({contact})" 
        else:
            LOGGER.info("No Contact to associate this Opportunity with")

        res = auth.post(url,data=json.dumps(payload))
        if res.status_code not in [200, 201]:
            raise Exception(f"{res.status_code} {res.reason} {res.text}")
        if res.status_code in [200, 201, 204] and payload.get("statecode") == 1:
            # Update the opportunites with Opportunity status
            oppurtunity_id = json.loads(res.text)["opportunityid"]
            opportunity_payload = {
                "Status": -1,
                "OpportunityClose":{
                "subject":"Won Opportunity",
                "opportunityid@odata.bind":f"/opportunities({oppurtunity_id})"
                }
                }
            res = auth.post(f"https://{self.config.get('org')}.crm.dynamics.com/api/data/v8.2/WinOpportunity", data = json.dumps(opportunity_payload))
        
        LOGGER.info(res.text)       

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        if self.stream_name in ["Contact", "Customers"] :
            self.contact_upload(record)
        if self.stream_name in ["Deals", "Opportunities"] :
            self.deals_upload(record)
        do="stuff"
        # Sample:
        # ------
        # client.write(record)
